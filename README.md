Q1
Write a recursive function pattern(n) which prints a number triangle. 
For example pattern(4) should print the following pattern on the terminal
1
21
321
4321

Q2
Write a recursive function fibonacciSeq(n) to print the fibonacci sequence up to n.
For example fibonacciSeq(10) should print the following numbers on the terminal.
0
1
1
2
3
5
8
13
21
34
55
