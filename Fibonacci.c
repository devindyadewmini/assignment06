#include <stdio.h>
int fibonacciSeq(int);

int fibonacciSeq(int z)
{
    if (z==0) return 0;
    else if (z==1) return 1;
    else return fibonacciSeq(z-1) + fibonacciSeq(z-2);
}

int main()
{
    int n,i;
    printf("Enter Ending Value for Fibonacci Sequence >>>> ");
    scanf("%d",&n);
    for (i=0; i<=n; i=i+1)
    {
        printf("%d \n",fibonacciSeq(i));
    }
}
